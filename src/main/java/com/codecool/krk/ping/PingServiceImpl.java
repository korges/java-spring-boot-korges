package com.codecool.krk.ping;

import org.springframework.stereotype.Service;

@Service
public class PingServiceImpl implements PingService {

    private PingRepository repository;

    public PingServiceImpl(PingRepository repository) {
        this.repository = repository;
    }

    @Override
    public Iterable<Ping> fingAll() {
        return this.repository.findAll();
    }

    @Override
    public void save(Ping ping) {
        this.repository.save(ping);
    }

    @Override
    public Ping findOne(Integer id) {
        return this.repository.findOne(id);
    }
}
