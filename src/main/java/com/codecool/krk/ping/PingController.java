package com.codecool.krk.ping;

import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path = "/ping")
public class PingController {

    private PingService service;

    public PingController(PingService service) {
        this.service = service;
    }

    @PostMapping(path = "")
    public Ping create(Ping ping) {
        this.service.save(ping);
        return ping;
    }

    @GetMapping(path = "")
    public Iterable<Ping> getPing() {
        return this.service.fingAll();
    }

    @GetMapping(path = "{id}")
    public Ping getOne(@PathVariable Integer id) {
        return this.service.findOne(id);
    }
}
