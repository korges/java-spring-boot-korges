package com.codecool.krk.ping;

public interface PingService {

    Iterable<Ping> fingAll();

    Ping findOne(Integer id);

    void save(Ping ping);
}
