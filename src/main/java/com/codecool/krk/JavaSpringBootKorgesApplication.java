package com.codecool.krk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaSpringBootKorgesApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaSpringBootKorgesApplication.class, args);
	}
}
